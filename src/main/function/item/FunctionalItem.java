package main.function.item;

import main.dto.item.ItemRating;
import main.model.item.Item;
import main.model.rent_request.Rating;
import main.model.rent_request.RentRequest;

import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface FunctionalItem {
    // 1. ------- Calculate topKRatedItems (by Anish Maharjan) --------
    Function<Item, Long> calculateRating = (item) ->
            Stream.of(item)
                    .flatMap(i -> i.getItemRentLines().stream())
                    .flatMap(itemRentLine -> itemRentLine.getRentRequests().stream())
                    .map(RentRequest::getRating)
                    .mapToLong(Rating::getItemRating)
                    .sum();

    BiFunction<List<Item>, Integer, List<Item>> topKRatedItems = (items, k) ->
            items.stream()
                    // ItemRating is a custom class to hold (Item, totalRating) tuple.
                    .map(item -> new ItemRating(item, calculateRating.apply(item)))
                    .sorted(Comparator.comparing(ItemRating::getSumOfRating).reversed()) // descending order
                    .limit(k)
                    .map(ItemRating::getItem)
                    .collect(Collectors.toList());
    // ------------------ End of Qsn 1 -----------------
}
