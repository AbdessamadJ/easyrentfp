package main.function.rent_request;


import main.dto.rent_request.ItemRejectionCount;
import main.dto.user.UserEarning;
import main.function.utils.TriFunction;
import main.model.item.Item;
import main.model.rent_request.RentRequest;
import main.model.user.User;
import main.model.utils.Status;

import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface FunctionalRentRequest {
    // 7. ---- Calculate topKRentersMadeMostMoneyInAYear (by Anish Maharjan) -----
    BiFunction<Item, Integer, Double> calculateEarningsOfAnItem = (item, year) ->
            Stream.of(item)
                    .flatMap(i -> i.getItemRentLines().stream())
                    .flatMap(itemRentLine -> itemRentLine.getRentRequests().stream())
                    // filtering RentRequest by year
                    .filter(rentRequest -> rentRequest.getFromDate().getYear()== year ||
                            rentRequest.getToDate().getYear() == year)
                    .mapToDouble(RentRequest::getPrice) // get only price from RentRequest
                    .sum(); // sum of all the price of a particular item

    TriFunction<List<Item>, Integer, Integer, List<User>> topKRentersMadeMostMoneyInAYear =
            (items, k, year) ->
                    items.stream()
                            // UserEarning is a custom class to hold (User, totalEarning) tuple.
                            .map(item -> new UserEarning(item.getCreatedBy(),
                                    calculateEarningsOfAnItem.apply(item, year)))
                            .sorted(Comparator.comparing(UserEarning::getTotalEarning).reversed())
                            .limit(k)
                            .map(UserEarning::getUser)
                            .collect(Collectors.toList());

    // ------------------ End of Qsn 7 -----------------


    // 13. ------- Calculate topKItemsHavingMostRentRequestRejection (by Anish Maharjan) --------
    Function<Item, Long> countTheRejectionStatusOfRentRequestOfAnItem = (item) ->
            Stream.of(item)
                    .flatMap(i -> i.getItemRentLines().stream())
                    .flatMap(itemRentLine -> itemRentLine.getRentRequests().stream())
                    .filter(rentRequest -> rentRequest.getStatus() == Status.Rejected)
                    .count();

    BiFunction<List<Item>, Integer, List<Item>> topKItemsHavingMostRentRequestRejection =
            (items, k) ->
                    items.stream()
                            .map(item -> new ItemRejectionCount(item,
                                    countTheRejectionStatusOfRentRequestOfAnItem.apply(item)))
                            .sorted(Comparator.comparing(ItemRejectionCount::getRejectionCount).reversed())
                            .limit(k)
                            .map(ItemRejectionCount::getItem)
                            .collect(Collectors.toList());

    // ------------------ End of Qsn 13 -----------------

    ///Komil - topKRatedRenter
    //Function<List<User>, Integer> topKRatedRenter = users -> users.stream().
}
