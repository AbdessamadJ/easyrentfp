package main.model.rent_request;

import main.model.item.ItemRentLine;
import main.model.user.User;
import main.model.utils.BaseEntity;
import main.model.utils.Status;

import java.time.LocalDateTime;

public class RentRequest extends BaseEntity {
    private long id;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private double price;
    private Status status;

    private ItemRentLine itemRentLine;
    private Rating rating;

    public RentRequest(User createdBy, LocalDateTime creationDate, User lastModifiedBy,
                       LocalDateTime lastModificationDate, User deletedBy, LocalDateTime deletionDate,
                       boolean deleted, long id, LocalDateTime fromDate, LocalDateTime toDate,
                       double price, Status status, ItemRentLine itemRentLine, Rating rating) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy,
                deletionDate, deleted);
        this.id = id;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.price = price;
        this.status = status;
        this.itemRentLine = itemRentLine;
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDateTime fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDateTime getToDate() {
        return toDate;
    }

    public void setToDate(LocalDateTime toDate) {
        this.toDate = toDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ItemRentLine getItemRentLine() {
        return itemRentLine;
    }

    public void setItemRentLine(ItemRentLine itemRentLine) {
        this.itemRentLine = itemRentLine;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }
}
