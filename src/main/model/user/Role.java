package main.model.user;


public class Role {
	private int id;
	private ERole name;

	public Integer getId() {
		return id;
	}

	public ERole getName() {
		return name;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(ERole name) {
		this.name = name;
	}
}