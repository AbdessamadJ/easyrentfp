
package main.model.item;

import main.model.rent_request.RentRequest;
import main.model.user.User;
import main.model.utils.BaseEntity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class ItemRentLine extends BaseEntity {
    private long id;
    private double price;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;

    private RentType rentType;
    private Set<RentRequest> rentRequests = new HashSet<>();
    private Item item;

    public ItemRentLine(User createdBy, LocalDateTime creationDate, User lastModifiedBy, LocalDateTime lastModificationDate, User deletedBy, LocalDateTime deletionDate, boolean deleted, long id, double price, LocalDateTime fromDate, LocalDateTime toDate,
                        RentType rentType, Set<RentRequest> rentRequests, Item item) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy, deletionDate, deleted);
        this.id = id;
        this.price = price;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.rentType = rentType;
        this.rentRequests = rentRequests;
        this.item = item;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDateTime getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDateTime fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDateTime getToDate() {
        return toDate;
    }

    public void setToDate(LocalDateTime toDate) {
        this.toDate = toDate;
    }

    public RentType getRentType() {
        return rentType;
    }

    public void setRentType(RentType rentType) {
        this.rentType = rentType;
    }

    public Set<RentRequest> getRentRequests() {
        return rentRequests;
    }

    public void setRentRequests(Set<RentRequest> rentRequests) {
        this.rentRequests = rentRequests;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
