package main.model.utils;

public enum Status {
    Pending,
    Accepted,
    Rejected
}
