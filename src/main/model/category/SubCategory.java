package main.model.category;

import main.model.user.User;
import main.model.utils.BaseEntity;

import java.time.LocalDateTime;

public class SubCategory extends BaseEntity {
    private long id;
    private String name;
    private String description;
    private String imagePath;
    private Category category;

    public SubCategory(User createdBy, LocalDateTime creationDate, User lastModifiedBy,
                       LocalDateTime lastModificationDate, User deletedBy, LocalDateTime deletionDate,
                       boolean deleted, long id, String name, String description, String imagePath,
                       Category category) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy,
                deletionDate, deleted);
        this.id = id;
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public Category getCategory() {
        return category;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
