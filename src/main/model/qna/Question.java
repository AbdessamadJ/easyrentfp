package main.model.qna;

import main.model.item.ItemRentLine;
import main.model.user.User;
import main.model.utils.BaseEntity;

import java.time.LocalDateTime;

public class Question extends BaseEntity {
    private long id;
    private Question parent;
    private String question;

    private ItemRentLine itemRentLine;

    public Question(User createdBy, LocalDateTime creationDate, User lastModifiedBy,
                    LocalDateTime lastModificationDate, User deletedBy, LocalDateTime deletionDate,
                    boolean deleted, long id, Question parent, String question,
                    ItemRentLine itemRentLine) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy,
                deletionDate, deleted);
        this.id = id;
        this.parent = parent;
        this.question = question;
        this.itemRentLine = itemRentLine;
    }

    public long getId() {
        return id;
    }

    public Question getParent() {
        return parent;
    }

    public String getQuestion() {
        return question;
    }

    public ItemRentLine getItemRentLine() {
        return itemRentLine;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setParent(Question parent) {
        this.parent = parent;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setItemRentLine(ItemRentLine itemRentLine) {
        this.itemRentLine = itemRentLine;
    }
}
