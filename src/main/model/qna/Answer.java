package main.model.qna;

import main.model.user.User;
import main.model.utils.BaseEntity;

import java.time.LocalDateTime;

public class Answer extends BaseEntity {
    private long id;
    private String answer;

    private Question question;

    public Answer(User createdBy, LocalDateTime creationDate, User lastModifiedBy, LocalDateTime lastModificationDate,
                  User deletedBy, LocalDateTime deletionDate, boolean deleted, long id,
                  Question question, String answer) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy,
                deletionDate, deleted);
        this.id = id;
        this.question = question;
        this.answer = answer;
    }

    public long getId() {
        return id;
    }

    public Question getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
